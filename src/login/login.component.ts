import {Component, Input} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormErrorService} from "../service/formErrors";
import Login from "../model/Login";

@Component({
    selector: 'login-component',
    templateUrl: './login.component.html'
})
export class LoginComponent {


    @Input()
    private login:Login = new Login();

    private loginForm: FormGroup;
    public formErrors = {
        email: '',
        password: '',
        remember: ''
    }
    constructor(
        public form: FormBuilder,
        public formErrorService: FormErrorService,
    ){}
    public signUp(){
            if (this.loginForm.valid){

            }else {
                this.formErrors = this.formErrorService.validateForm(this.loginForm, this.formErrors, false)
            }

    }

        public buildForm(){
        this.loginForm = this.form.group({
            email: ['', Validators.email],
            password: ['', [Validators.required]],
            remember: ['']
        });
        this.loginForm.valueChanges.subscribe((data) => {
            this.formErrors = this.formErrorService.validateForm(this.loginForm, this.formErrors, true)
        })
    }
        public ngOnInit(){
                this.buildForm();
        }

    private submitLoginForm(){
        console.log(this.loginForm);
        this.login = this.loginForm.value;
        console.log(this.login);

}
}