import {Component} from "@angular/core";

@Component({
    selector: 'navbar-component',
    templateUrl: './navbar.component.html'
})
export class NavbarComponent {
    private user;
    constructor(){
        this.user = {
            name: 'Ján',
            surname: 'Zákutný'
        }
    }

}

