export  default class Workshop{
    public name: String;
    public description: String;
    public language: String;
    public startDate: Date;
    public lessons: number;
};