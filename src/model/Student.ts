import Workshop from "./Workshop";

export default class Student {

    public name: String;
    public workshop:Workshop;
    public email:String;
    public employmentType: String;
}