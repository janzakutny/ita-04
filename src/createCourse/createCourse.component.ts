import {Component} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {isNumber} from "ngx-bootstrap/timepicker/timepicker.utils";
import {CustomValidators} from "../service/customFormValidator";
import {FormErrorService} from "../service/formErrors";

@Component({
    selector: 'create-course-component',
    templateUrl: './createCourse.component.html'
})
export class CreateCourseComponent {

    private courseForm: FormGroup;
    public formErrors = {
        name: '',
        description: '',
        startDate: '',
        numberOfLessons: '',
        language: '',
    }
    constructor(
        public fb: FormBuilder,
        public formErrorService: FormErrorService,
    ){}
    public signUp(){
            if (this.courseForm.valid){

            }else {
                this.formErrors = this.formErrorService.validateForm(this.courseForm, this.formErrors, false)
            }
    }

    public buildForm(){
        this.courseForm = this.fb.group({
            name: ['', [Validators.required, CustomValidators.validateCharacters]],
            description: ['', Validators.required],
            startDate: [ '',[Validators.required]],
            numberOfLessons: [isNumber, Validators.required],
            language: ['', Validators.required]
        });
        this.courseForm.valueChanges.subscribe((data) => {
            this.formErrors = this.formErrorService.validateForm(this.courseForm, this.formErrors,true)
        })
    }
    public ngOnInit(){
        this.buildForm();
    }
}