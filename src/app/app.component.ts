import {Component, Input, OnInit,ViewEncapsulation} from "@angular/core";
import Workshop from "../model/Workshop";
import Student from "../model/Student";
import Login from "../model/Login";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['../../public/styles/styles.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit{

    @Input()
    private  workshop:Workshop = new Workshop();
    private student = new Student();
    private login = new Login();

    constructor(){
        console.log(this.workshop);
        console.log(this.student);
        console.log(this.login);


    }
    ngOnInit(): void {
        console.log(this.student);
        console.log(this.workshop);
        console.log(this.login);
    }
}
