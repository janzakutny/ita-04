import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {NavbarComponent} from "../navbar/navbar.component";
import {SidebarComponent} from "../sidebar/sidebar.component";
import {ContentComponent} from "../content/content.component";
import {LoginComponent} from "../login/login.component";
import {CourseDetailLecturerComponent} from "../courseDetailLecturer/courseDetailLecturer.component";
import {CourseDetailStudentComponent} from "../courseDetailStudent/courseDetailStudent.component";
import {CreateCourseComponent} from "../createCourse/createCourse.component";
import {CreateLecturerComponent} from "../createLecturer/createLecturer.component";
import {CreateStudentComponent} from "../createStudent/createStudent.component";
import {BsDropdownModule} from "ngx-bootstrap/dropdown";
import {DemoDropdownBasicComponent} from "../dropDownMenu/dropDownMenu.component";
import {DemoDropdownBasicBellComponent} from "../dropDownMenuBell/dropDownMenuBell.component";
import {DemoDropdownBasicFlagComponent} from "../dropDownMenuFlag/dropDownMenuFlag.component";
import {ReactiveFormsModule} from "@angular/forms";
import {FormErrorService} from "../service/formErrors";

@NgModule({
    imports: [
        BrowserModule,
        BsDropdownModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [
        AppComponent,
        NavbarComponent,
        SidebarComponent,
        ContentComponent,
        LoginComponent,
        CourseDetailLecturerComponent,
        CourseDetailStudentComponent,
        CreateCourseComponent,
        CreateLecturerComponent,
        CreateStudentComponent,
        DemoDropdownBasicComponent,
        DemoDropdownBasicBellComponent,
        DemoDropdownBasicFlagComponent
    ],
    providers: [
        FormErrorService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
