import {Component, Input} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import Student from "../model/Student";
import Workshop from "../model/Workshop";
import {FormErrorService} from "../service/formErrors";
import {CustomValidators} from "../service/customFormValidator";


@Component({
    selector: 'create-student-component',
    templateUrl: './createStudent.component.html'
})
export class CreateStudentComponent{
    @Input()
    private studentForm: FormGroup;
    private student:Student = new Student();

    private workshops:Workshop[] = [{
        name: 'ITA 04 - Angular 2',
        description: 'Angular 2 workshop description',
        language: 'javascript',
        startDate: new Date(),
        lessons: 10
    },{
        name: 'ITA 04 - React',
        description: 'React workshop description',
        language: 'javascript',
        startDate: new Date(),
        lessons: 10
    },{
        name: 'ITA 04 - C#',
        description: 'C# workshop description',
        language: 'C#',
        startDate: new Date(),
        lessons: 10
    }];

    public formErrors = {
        name: '',
        workshop: '',
        email: '',
        employmentType: '',
    }

    constructor(
        public fb: FormBuilder,
        public formErrorService: FormErrorService,
    ){}
    public signUp() {
            if (this.studentForm.valid){

            }else {
                this.formErrors = this.formErrorService.validateForm(this.studentForm, this.formErrors, false)
            }
    }

    public buildForm(){
        this.studentForm = this.fb.group({
            name: ['', [Validators.required, CustomValidators.validateCharacters]],
            workshop: ['', Validators.required],
            email: ['', Validators.email],
            employmentType: ['', Validators.required]
        });
        this.studentForm.valueChanges.subscribe((data) => {
            this.formErrors = this.formErrorService.validateForm(this.studentForm, this.formErrors, true)
        })
    }
    public ngOnInit(){
        this.buildForm();
    }
    private submit(){
        console.log(this.studentForm);
        this.student = this.studentForm.value;
        console.log(this.student);
    }
}





